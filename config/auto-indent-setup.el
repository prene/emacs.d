(require-package 'auto-indent-mode)

(after 'prog-mode (auto-indent-mode t))
